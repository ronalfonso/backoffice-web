import React from 'react';
import {Provider} from "react-redux";
import {store} from "./store/store";
import AppRouter from "./router/AppRouter";

import './assets/styles/customTheme.scss';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';
import 'primeflex/src/_variables.scss';
import 'primeflex/src/_elevation.scss';

import { library } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
import {
    faChartPie, faChild, faCogs,
    faPlusSquare,
    faTachometerAlt
} from "@fortawesome/free-solid-svg-icons";

library.add(
    fab,
    faPlusSquare,
    faTachometerAlt,
    faChartPie,
    faCogs,
    faChild
)

const BackofficeApp = () => {
    return (
        <>
            <Provider store={store}>
                <AppRouter />
            </Provider>
        </>
    );
};

export default BackofficeApp;

import {environment} from "../environments/environment.dev";
import axios from "axios";
import {types} from "../types/types";
const baseUrl = process.env.REACT_APP_API_URL;

const fetchOutToken = ( endpoint, data, method = 'GET', headers) => {

    const url = `${ environment.API_BASE_URL }/${ endpoint }`;

    switch (method) {
        case types.get:
           return axios.get(url);

        case types.put:
            return axios.put(url, data, {headers})
                .then(resp => {
                    if (resp.data.hasOwnProperty('passwordHash')) {delete resp.data.passwordHash;}
                    return resp.data;
                })
                .catch(err => {
                    console.log(err);
                })

        case types.post:
            return axios.post(url, data, {headers})
                .then(resp => {
                    if (resp.data.hasOwnProperty('passwordHash')) {delete resp.data.passwordHash;}
                    return resp.data;
                })
                .catch(err => {
                    console.log(err);
                })
        default:
            return;
    }
}

const fetchWithToken = ( endpoint, data, method = 'GET' ) => {

    const url = `${ baseUrl }/${ endpoint }`;
    const token = localStorage.getItem('token') || '';

    if ( method === 'GET' ) {
        return fetch( url, {
            method,
            headers: {
                'x-token': token
            }
        });
    } else {
        return fetch( url, {
            method,
            headers: {
                'Content-type': 'application/json',
                'x-token': token
            },
            body: JSON.stringify( data )
        });
    }
}


export {
    fetchOutToken,
    fetchWithToken
}

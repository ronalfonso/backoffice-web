import React from 'react';
import ReactDOM from 'react-dom';

import BackofficeApp from "./BackofficeApp";

import './styles.scss';

ReactDOM.render(
    <BackofficeApp />,
  document.getElementById('root')
);


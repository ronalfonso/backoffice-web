import React from 'react';

import { DataTable } from 'primereact/datatable';
import {Column} from "primereact/column";

const Tables = ({data, columns}) => {


    const dynamicColumns = columns.map((col,i) => {
        return <Column key={col.field} field={col.field} header={col.header} />;
    });

    return (
        <>
            <DataTable value={data}>
                {dynamicColumns}
            </DataTable>
        </>
    );
};

export default Tables;

import React, {useEffect, useState} from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSignOutAlt, faSortDown, faUser} from "@fortawesome/free-solid-svg-icons";
import {useDispatch, useSelector} from "react-redux";
import {logout} from "../../../actions/auth";
import {Check} from "is-null-empty-or-undefined";
import {useHistory} from "react-router-dom";


const Topbar = () => {
    let history = useHistory();
    const [dropdown, setDropdown] = useState(false);
    const dispatch = useDispatch();
    const noAuth = history.location.pathname.includes('auth');
    const {user: {email, firstName, lastName}} = useSelector( state => state.auth);

    const handleLogout = (e) => {
        e.preventDefault();
        dispatch(logout());
    }

    useEffect(() => {
        const mouseClick = ({target}) => {
            if (!Check(document.querySelector('#card_logout'))) {
                if (!document.getElementById('card_logout').contains(target)) {
                    setDropdown(false);
                }
            }
        }
        if (!noAuth) {
            window.addEventListener('click', mouseClick)
        }
        return () => {
            window.removeEventListener('click', mouseClick)
        }

    }, [noAuth])


    return (
        <div className="layout__topbar">
            <div className="left__topbar">

            </div>
            <div id="card_logout" className="right__topbar">
                <div className="dropdown_logout_container">
                    <div
                        className="button_dropdown"
                        onClick={() => setDropdown(!dropdown)}
                    >
                        <div>
                            <FontAwesomeIcon icon={faUser}/>
                        </div>
                        <div style={{marginTop: -5, marginLeft: 4}}>
                            <FontAwesomeIcon icon={faSortDown}/>
                        </div>
                    </div>
                    <div
                        className={`card_logout p-shadow-2 ${dropdown ? 'display-block' : 'display-none'}`}
                    >
                        <div className="card_logout_header">
                            <span>{firstName} {lastName}</span>
                            <span>{email}</span>
                        </div>
                        <hr/>
                        <div className="button_logout_container">
                            <div
                                className="button_logout"
                                onClick={handleLogout}
                            >
                                <FontAwesomeIcon icon={faSignOutAlt}/>
                                <span>Cerrar sesión</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Topbar;

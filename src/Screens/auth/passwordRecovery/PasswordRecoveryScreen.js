import React from 'react';
import {Button} from "primereact/button";
import {InputText} from "primereact/inputtext";
import {useDispatch} from "react-redux";
import {useForm} from "../../../hooks/useForm";
import {login} from "../../../actions/auth";

const PasswordRecoveryScreen = () => {

    const dispatch = useDispatch();

    const [formValues, handleInputChange] = useForm({
        usernameOrEmail: ''
    });

    const {usernameOrEmail} = formValues;

    const handleRecovery = (e) => {
        e.preventDefault();
        dispatch(login({
                id: '123',
                firstName: 'Ronald',
                lastName: 'Alfonso',
                jwt: 'token'
            })
        )
    }

    return (
        <form className="auth__form">

            <h3>¿Olvidó su contraseña?</h3>

            <div className="p-inputgroup">
                    <span className="p-float-label">
                        <InputText
                            id="username"
                            name="usernameOrEmail"
                            value={usernameOrEmail}
                            onChange={handleInputChange}
                        />
                        <label htmlFor="username">Ingrese su correo electronico</label>
                    </span>
            </div>

            <Button
                disabled={usernameOrEmail.trim().length === 0}
                className="button-large"
                type="submit"
                onClick={handleRecovery}
                label="Enviar correo de recuperacion"
            />

        </form>
    );
};

export default PasswordRecoveryScreen;

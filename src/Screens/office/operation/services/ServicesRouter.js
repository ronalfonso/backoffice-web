import React from 'react';
import {
    Switch,
    Route,
    Redirect
} from "react-router-dom";
import ServicesScreen from "./list/ServicesScreen";

const ServicesRouter = () => {
    return (
        <div className="view-main">
                <Switch>

                    <Route
                        exact
                        path="/services"
                        component={ServicesScreen}>
                    </Route>

                    <Redirect to="/dashboard" />

                </Switch>
        </div>

    );
};

export default ServicesRouter;

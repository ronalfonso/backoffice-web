import {types} from "../types/types";
import {fetchOutToken} from "../helpers/fetch";
import Swal from "sweetalert2";
import {finishLoading, startLoading} from "./ui";

const classUrl = 'LoginREST/rest/';

export const startLoginUserOrEmail = (usernameOrEmail, password) => {
    Swal.fire({
        icon: 'info',
        text: 'Por favor espere...',
        allowOutsideClick: false,
    })
    Swal.showLoading();
    return async (dispatch) => {
        const endpoint = classUrl + `login/administrator/${usernameOrEmail}`
        const headers = {
            password: password,
            app: 'APP_BCK',
            'Content-Type': 'application/json'
        }
        const user = {
                        userId: 1234,
                        profile: {},
                        role: 'Admin',
                        permissions: [],
                        businessUnit: {},
                        businessUnitDefault: {},
                        email: 'raalfonsoparra@gmail.com',
                        firstName: 'Ronald',
                        lastName: 'Alfonso',
                        username: 'ronalfonso',
                        tenantTheme: {}
                    }
                for (let userKey in user) {
                    localStorage.setItem(userKey, JSON.stringify(user[userKey]))
                }
        dispatch(login(user))
        dispatch(startLoading());


    }
}

export const login = (user) => {
    Swal.close();
    return {
        type: types.login,
        payload: user
    }
}

export const logout = () => {
    localStorage.clear();
    return {
        type: types.logout
    }
}
